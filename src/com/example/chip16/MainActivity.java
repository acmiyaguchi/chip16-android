package com.example.chip16;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.content.Intent;
import android.widget.EditText;



import java.io.File;
import android.view.MenuItem;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;


import com.example.chip16.Cpu;
import com.example.chip16.FileChooser;

public class MainActivity extends Activity {
    private static final String TAG = "MAINACTIVITY";
    private static final int REQUEST_BROWSE_ROM = 1;


    private static Cpu emulator = null;
    private static Thread emuThread = null;
    private static int resumeRequested = 0;

    private boolean isMenuShowing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        File datadir = getDir("data", MODE_PRIVATE);
        if(!initEmulator(datadir)) {
            Log.e(TAG, "Could not initialize emulator.");
            finish();
            return;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isFinishing()) {
            resumeRequested = 0;
            emulator = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);

        if (isMenuShowing) {
            isMenuShowing = false;
            resumeEmulator();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (isMenuShowing) {
            isMenuShowing = false;
            resumeEmulator();
        }

        switch (item.getItemId()) {
            case R.id.menu_open:
                onLoadROM();
                return true;
            /*
            case R.id.menu_reset:
                emulator.reset();
                return true;

            case R.id.menu_save_state:
                showDialog(DIALOG_SAVE_STATE);
                return true;

            case R.id.menu_load_state:
                showDialog(DIALOG_LOAD_STATE);
                return true;

            case R.id.menu_close:
                unloadROM();
                return true;
            */
            case R.id.menu_quit:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onLoadROM() {
        Intent intent = new Intent(this, FileChooser.class);
        intent.putExtra(FileChooser.EXTRA_TITLE,
                getResources().getString(R.string.title_select_rom));
        //intent.putExtra(FileChooser.EXTRA_FILEPATH, lastPickedGame);
        intent.putExtra(FileChooser.EXTRA_FILTERS,
                new String[] { ".c16" });
        startActivityForResult(intent, REQUEST_BROWSE_ROM);
    }

    private boolean initEmulator(File datadir) {
        if(emulator != null)
            return true;

        emulator = new Cpu();
        emulator.initCPU();

        // Possibly initialize the emulator before threading

        if(emuThread == null) {
            emuThread = new Thread() {
                public void run() {
                    emulator.run();
                }
            };
            emuThread.start();
        }
        return true;
    }

    private void pauseEmulator() {
        if(--resumeRequested == 0)
            emulator.stop();
    }

    private void resumeEmulator() {
        if(resumeRequested++ == 0)
            emulator.start();
    }
}
